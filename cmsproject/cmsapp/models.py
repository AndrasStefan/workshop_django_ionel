from django.db import models

from binascii import hexlify

from os import urandom

# Create your models here.
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

def make_default_slug():
    return hexlify(urandom(16))


class Page(models.Model):
    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')

    slug = models.SlugField(max_length=200, default=make_default_slug, unique=True)
    title = models.CharField(max_length=200)
    content = models.TextField()

    is_draft = models.BooleanField(default=False)
    revision_of = models.OneToOneField("self", null=True,
                                       related_name="pending_draft",
                                       on_delete=models.CASCADE)

    def __repr__(self):  # https://pyformat.info/
        return "<Page #{0.pk} slug={0.slug!r} title={0.title!r}" \
               " content={0.content!r:.5}>".format(self)

    def get_absolute_url(self):
        #return reverse('cmsapp:page', kwargs={'slug': self.slug})
        return reverse('cmsapp:index')


class Tag(models.Model):
    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    name = models.CharField(max_length=200)
    page = models.ForeignKey("Page",
                             on_delete=models.CASCADE,
                             related_name='tags',
                             related_query_name='tag'
                             )


class SidebarPage(Page):  # or TodoPage
    sidebar_title = models.CharField(max_length=200)


class SidebarItem(models.Model):
    sidebar_page = models.ForeignKey("SidebarPage",
                                     related_name='sidebar_items',
                                     related_query_name='sidebar_item')
    content = models.TextField()

from django.contrib import admin

from .models import Page, SidebarItem, SidebarPage


# Register your models here.


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = 'pk', 'title', 'trimmed_content'
    list_editable = 'title',

    def trimmed_content(self, obj):
        return obj.content[:10]


class SidebarItemInline(admin.TabularInline):
    model = SidebarItem


@admin.register(SidebarPage)
class SidebarPageAdmin(PageAdmin):
    inlines = SidebarItemInline,

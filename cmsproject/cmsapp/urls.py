from . import views

from django.conf.urls import url

app_name = 'cmsapp'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^\+$', views.genericpage_change, name='change'),
    url(r'^\+sidebar$', views.genericpage_change, {'kind': 'sidebar'}, name='add-sidebar'),
    url(r'^:(?P<tag>[^/]+)/$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<slug>[^/]+)/$', views.PageView.as_view(), name='page'),
    url(r'^(?P<slug>[^/]+)/edit$', views.genericpage_change, name='change'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/sidebaredit$', views.genericpage_change,
    {'kind': 'sidebar'},
    name='change-sidebar'),
    #url(r'^moderate/$', views.ModerateIndexView.as_view(), name='moderate'),
    #url(r'^moderate/(?P<pk>[0-9]+)/$', views.page_moderate, name='moderate'),
]
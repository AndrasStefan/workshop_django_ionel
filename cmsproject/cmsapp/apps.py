import os
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CmsappConfig(AppConfig):
    name = 'cmsapp'
    verbose_name = _('CMS')

    def ready(self):
        print("===== Django is ready! (pid:%s)" % os.getpid())

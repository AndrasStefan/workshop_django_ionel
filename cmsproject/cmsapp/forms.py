from .models import Page, Tag, SidebarPage, SidebarItem
from django import forms


class TagField(forms.CharField):
    def clean(self, value):
        value = super(TagField, self).clean(value)
        return [item.strip() for item in value.split(',') if item.strip()]


class PageForm(forms.ModelForm):

    def __init__(self, data=None, *, initial=None, instance=None,
                 **kwargs):
        if instance:
            initial = initial or {}
            initial['tags'] = ', '.join(
                instance.tags.values_list('name', flat=True))
        super(PageForm, self).__init__(
            data, initial=initial, instance=instance, **kwargs)

    def save(self, commit=True):
        instance = super(PageForm, self).save(commit)
        if commit:
            self.instance.tags.all().delete()
            Tag.objects.bulk_create([
                Tag(name=tag, page=self.instance)
                for tag in self.cleaned_data['tags']])
        return instance

    class Meta:
        model = Page
        fields = 'title', 'tags', 'content'

    tags = TagField(required=False)


class DeletePage(forms.Form):
    delete = forms.BooleanField(label="Yes, i am sure", required=False)

    def clean(self):
        cleaned_data = super(DeletePage, self).clean()
        if not cleaned_data.get('delete'):
            raise forms.ValidationError('Must confirm the delete')
        return cleaned_data


from djangoformsetjs.utils import formset_media_js
class SidebarItemForm(forms.ModelForm):
    class Media:
        js = formset_media_js


class SidebarPageForm(PageForm):
    class Meta:
        model = SidebarPage
        fields = "slug", "title", "tags", "content", "sidebar_title"


SidebarItemFormset = forms.inlineformset_factory(
    SidebarPage, SidebarItem, fk_name='sidebar_page',
    fields=["content"],
)
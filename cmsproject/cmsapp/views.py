from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.transaction import atomic
from django.shortcuts import get_object_or_404, redirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView

from .forms import PageForm, DeletePage, SidebarPageForm, SidebarItemFormset
from .models import Page, Tag, SidebarPage


# Create your views here.

class IndexView(ListView):
    #model = Page
    queryset = Page.objects.filter(is_draft=False)
    paginate_by = 10
    ordering = 'pk'

    def get_context_data(self):
        return super(IndexView, self).get_context_data(
            tags=Tag.objects.values('name').distinct()
        )

    def get_queryset(self):
        qs = super(IndexView, self).get_queryset()
        if 'tag' in self.kwargs:
            return qs.filter(tag__name=self.kwargs['tag'])
        else:
            return qs


class PageView(DetailView):

    model = Page
    slug_url_kwarg = 'slug'  # this is by default

    def get_context_data(self, object):
        return super(PageView, self).get_context_data(
            page=object,
            tags=object.tags.all(),
            sidepage=getattr(object, 'sidebarpage', None),
        )


@method_decorator(login_required, name='dispatch')
class PageAddView(CreateView):
    model = Page
    form_class = PageForm


def index(request, tag=None):
    if tag is None:
        pages = Page.objects.all()
    else:
        pages = Page.objects.filter(tag__name=tag)

    return render(request, "cmsapp/index.html", {
        'pages': pages,
        'tags': Tag.objects.values('name').distinct(),
    })


@login_required
def page(request, slug):
    pg = get_object_or_404(Page, slug=slug)

    if request.method == "POST":
        form = DeletePage(request.POST)
        if form.is_valid():
            pg.delete()
            return redirect("cmsapp:index")
    else:
        form = DeletePage()

    return render(request, "cmsapp/page.html", {
        'page': pg,
        'tags': pg.tags.all(),
        'form': form,
    })


def change(request, slug=None):
    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(Page, slug=slug)

    if request.method == "POST":
        form = PageForm(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            return redirect("cmsapp:index")
    else:
        form = PageForm(instance=instance)

    return render(request, "cmsapp/change.html", {
        'form': form, 'page': instance,
        'action': reverse('cmsapp:change',
                          kwargs=instance and {'slug': instance.slug})
        })


@atomic
def sidebarpage_change(request, slug=None):
    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(SidebarPage, slug=slug)

    if request.method == "POST":
        form = SidebarPageForm(request.POST, instance=instance)
        formset = SidebarItemFormset(request.POST, instance=form.instance)
        if form.is_valid() and formset.is_valid():
            instance = form.save()
            formset.save()
            return redirect("cmsapp:page", slug=instance.slug)
    else:
        form = SidebarPageForm(instance=instance)
        formset = SidebarItemFormset(instance=instance)
    return render(request, "cmsapp/sidebarpage_form.html", {
        'form': form,
        'formset': formset,
    })


def genericpage_change(request, slug=None, kind='page'):
    if kind == 'page':
        Model = Page
    elif kind == 'sidebar':
        Model = SidebarPage
    else:
        raise RuntimeError('Unexpected kind %r' % kind)

    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(Page, slug=slug)
        if getattr(instance, 'sidebarpage', None):
            kind = 'sidebar'
            instance = instance.sidebarpage

    if kind == 'page':
        Form = PageForm
        Formset = None
    elif kind == 'sidebar':
        Form = SidebarPageForm
        Formset = SidebarItemFormset
    else:
        raise RuntimeError('Unexpected kind %r' % kind)

    pending_draft = getattr(instance, 'pending_draft', None)
    if pending_draft:
        return redirect("cmsapp:moderate", pk=pending_draft.pk)

    if request.method == "POST":
        draft = Model(is_draft=True, revision_of=instance)
        form = Form(request.POST, instance=draft)
        if Formset:
            formset = Formset(request.POST)
        else:
            formset = None

        if form.is_valid() and (not formset or formset.is_valid()):
            form.save()
            if formset:
                for item in formset.save(commit=False):
                    item.sidebar_page = draft
                    item.pk = None
                    item.save()
            messages.add_message(
                request, messages.INFO,
                ("Your page {title!r} was saved and "
                  "it's waiting moderation. A moderator can publish it.").format(
                    title=draft.title))
            return redirect("cmsapp:index")
    else:
        form = Form(instance=instance)
        if Formset:
            formset = Formset(instance=form.instance)
        else:
            formset = None
    return render(request, "cmsapp/sidebarpage_form.html", {
        'form': form,
        'formset': formset,
    })


class ModerateIndexView(ListView):
    queryset = Page.objects.filter(is_draft=True)
    paginate_by = 10
    ordering = 'pk'
    template_name_suffix = '_moderate_list'
# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-03 07:05
from __future__ import unicode_literals

import cmsapp.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsapp', '0015_make_unique_slugs'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='slug',
            field=models.SlugField(default=cmsapp.models.make_default_slug, max_length=200, unique=True),
        ),
    ]

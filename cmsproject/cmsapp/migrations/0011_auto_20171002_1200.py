# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-02 12:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsapp', '0010_auto_20171002_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='slug',
            field=models.SlugField(default=b'3e9678d21915d0a51743e97b06408a48', max_length=200),
        ),
    ]

import pytest
from cmsapp.models import Page
from django.http import HttpResponse
from django.test import Client
from cmsapp.forms import PageForm

@pytest.fixture
def authed_client(db, client, admin_user):
    client.force_login(admin_user)
    return client

@pytest.fixture
def pages(request, db):
    return [
        Page.objects.create(slug='abc', title='ABC',
                            content='lorem ipsum'),
        Page.objects.create(slug='123', title='1 2 3',
                            content='lorem ipsum'),
        Page.objects.create(slug='foo', title='Bar',
                            content='lorem ipsum'),
    ]


def test_index(pages, client: Client):
    resp: HttpResponse = client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['pages']) == pages
    content = resp.content.decode(resp.charset)
    for page in pages:
        assert page.title in content


def test_tag(db, client: Client):
    resp: HttpResponse = client.get('/:a/')
    assert resp.status_code == 200
    content = resp.content.decode(resp.charset)
    assert 'No pages yet' in content


def test_index_empty(db, client):
    resp = client.get('/')
    assert resp.status_code == 200

    assert len(resp.context['pages']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No pages' in content


def test_show(pages, client):
    resp = client.get('/abc/')
    assert resp.status_code == 200
    assert resp.context['page'] == pages[0]
    content = resp.content.decode(resp.charset)
    assert 'lorem ipsum' in content


def test_show_redirect(pages, client):
    resp: HttpResponse = client.get('/abc')
    assert resp.status_code == 301
    assert resp.url == '/abc/'


def test_add(db, client):
    resp = client.post('/+', {'title': 'Foo',
                              'slug': 'foo',
                              'content': 'Baaaar',
                              'tags': 't1, t2'})
    content = resp.content.decode(resp.charset)
    print(content)
    assert resp.status_code == 302
    assert resp.url == '/'
    resp = client.post(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Foo' in content
    assert 'href="/:t1/"' in content
    assert 'href="/:t2/"' in content


def test_edit(pages, client):
    resp = client.get('/foo/edit')
    assert resp.status_code == 200
    data = resp.context['form'].initial
    data['title'] = 'New Title'

    resp = client.post('/foo/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/'

    resp = client.post(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'New Title' in content
    print(content)


def test_delete(pages, authed_client):
    pg = Page.objects.create(slug='slugdetest', title='', content='')

    resp = authed_client.get('/slugdetest/')
    assert resp.status_code == 200

    resp = authed_client.post('/slugdetest/', )
    assert resp.status_code == 200

    resp = authed_client.post('/slugdetest/', {'delete': True})
    assert resp.status_code == 302


def test_form(db):
    p = Page.objects.create(slug='abc', title='ABC', content='lorem ipsum')
    data = {'slug': p.slug, 'title': p.title, 'content': p.content}
    form = PageForm(data=data)
    assert form.is_valid()
    new = form.save(commit=False)
    assert new.title == p.title


def test_page_repr():
    assert repr(Page(slug=1, title=2, content=2)) == '<Page #None slug=1 title=2 content=2>'


@pytest.mark.parametrize('data', [
    {'title': '', 'slug': 'bar', 'content': 'lorem ipsum'},
    {'title': 'foo', 'slug': 'bar', '': 'lorem ipsum'},
    {'title': 'foo', 'slug': 'bar', 'content': ''},
])
def test_add_empty(db, client, data):
    resp = client.post('/+', data)
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content


def test_page_admin(pages, client: Client, admin_user):
    client.force_login(admin_user)
    resp = client.get('/admin/cmsapp/page/')
    assert resp.status_code == 200
